<!doctype html>
<html lang="pt">
<head>
    <meta charset="utf-8">
    <title>Programação</title>
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="css/styles.css">
    <!-- Latest compiled and minified CSS Bootstrap-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">

</head>

<body>


<?php
date_default_timezone_set('America/Sao_Paulo');
define( 'PARSE_SDK_DIR', './Parse/' );
require_once('vendor/autoload.php');
use Parse\ParseClient;
use Parse\ParseQuery;
ParseClient::initialize('K01Op0Qn3IWAjMOyguYrlkOg7HxzHiSn0JP31sIt', 'TDXrmA2mBT3LsCrqC6dzLM9QjDuQmGNdRcxLrJGx', '8ExYRNPAI2ibdiZFdciJBgBzPsWNO17uVEm1MEX5');
$query = new ParseQuery("Talks");
$innerquery = new ParseQuery("Events");
$innerquery->equalTo('objectId','9WsVbjGiXt');
$query->matchesQuery("event", $innerquery);
$query->includeKey("event");
$count = $query->count();
echo '<h2>Número de atividades registradas no Acelera Serra: ' . $count . '</h2>';
echo '<h1>Atividades</h1>';
$query->limit(1000);
$query->ascending("date_talk");
$results = $query->find();
echo '<table><thead><tr><th>ID</th><th>Data</th><th>Hora</th><th>Título</th><th>Local</th></tr></thead><tbody>';
foreach ( $results as $result ) {
    echo '<tr><td>' . $result->getobjectId() . '</td><td>'. $result->get('date') .'</td><td>'. $result->get('start_hour').'</td><td>'. $result->get('title').'</td><td>'. $result->get('local').'</td></tr>';
}
echo '</tbody></table>';
?>

</body>