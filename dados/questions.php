<!doctype html>
<html lang="pt">
<head>
    <meta charset="utf-8">
    <title>Perguntas</title>
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="css/styles.css">
    <!-- Latest compiled and minified CSS Bootstrap-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">

</head>

<body>


<?php
date_default_timezone_set('America/Sao_Paulo');
define( 'PARSE_SDK_DIR', './Parse/' );
require_once('vendor/autoload.php');
use Parse\ParseClient;
use Parse\ParseQuery;
ParseClient::initialize('K01Op0Qn3IWAjMOyguYrlkOg7HxzHiSn0JP31sIt', 'TDXrmA2mBT3LsCrqC6dzLM9QjDuQmGNdRcxLrJGx', '8ExYRNPAI2ibdiZFdciJBgBzPsWNO17uVEm1MEX5');
$query = new ParseQuery("Questions");
$innerquery = new ParseQuery("Talks");
$innerquery->containedIn('objectId',
    [   "9pQSOP7ycN",
        "b065uLEl9Q",
        "QvrXZwWabR",
        "U1B7YDanXx",
        "J82yIoxkS1",
        "fm1puqlfue",
        "uMza1XTk2v",
        "KG9pN87Oj4",
        "aBD5mxk2a7",
        "OPVWjlh5E5",
        "vtcPLEdFcp",
        "GKhgaex4Sr",
        "8kCL1RcuKQ",
        "Fqsidtl28i",
        "0JN75Zaa6d",
        "zBIMZIYIJ4",
        "SgoJxnZ6tE",
        "IOO9VU5ldU",
        "mz1JqHz2iX",
        "Bd48InTg0F",
        "VE6gYuHuDK",
        "1qy9ws5E8l",
        "NFIh6LU0o5",
        "UNv0PbCwG6"
]);
$query->matchesQuery("talk", $innerquery);
$query->includeKey("questioning");
$query->includeKey("talk");
$query->includeKey("talk.event");
$count = $query->count();
echo '<h2>Número de perguntas em todas palestras do Acelera Serra: ' . $count . '</h2>';
echo '<h1>Perguntas recentes</h1>';
$query->limit(1000);
$query->descending("createdAt");
$results = $query->find();
echo '<table><tr><th width="15%">Criado em</th><th>Pergunta</th><th>Autor</th><th>Palestra</th></tr>';
foreach ( $results as $result ) {
    echo '<tr><td>' . $result->getCreatedAt()->format('Y-m-d H:i') . '</td><td>'. $result->get('question') .'</td><td>'. $result->get('questioning')->get('username').'</td><td>'. $result->get('talk')->get('title') . '</td></tr>';
}
echo '</table>';
?>

</body>

