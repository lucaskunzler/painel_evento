      Parse.initialize("K01Op0Qn3IWAjMOyguYrlkOg7HxzHiSn0JP31sIt", "vCoLgj60axmnn70AVJmL75JKDHx0sZALYGzzDFQx");
 

      var Questions = Parse.Object.extend("Questions");
      var Events = Parse.Object.extend("Events");
      var Talks = Parse.Object.extend("Talks");
      var innerQuery = new Parse.Query(Events);

    function getQuestions(){
      var query = new Parse.Query(Questions);

                query.equalTo("active", true);
                query.descending("createdAt");
                query.include("questioning");
                query.include("talk");

                var localQuery = new Parse.Query(Talks);
                localQuery.equalTo("objectId","uMza1XTk2v");

                query.matchesQuery("talk",localQuery);

                query.find({

        success: function (results){
          var output = "";
            for (var i in results){
                var talk = results[i].get("talk");
                var title = talk.get("title");
                var question = results[i].get("question");
                var questioning = results[i].get("questioning");
                var full_name = questioning.get("full_name");
                var email = questioning.get("email");

                output += "<div class='question'><h1>"+question+"</h1>";
                output += "<h2><strong>Usuário: </strong>"+full_name+"</h2>";
                output += "<p><strong>Palestra: </strong>"+title+"</p></div>";
            }
            $("#questions").html(output);
        }, error: function (error){
            console.log("Query Error:"+error.message);
        }
      });
    }


    getQuestions();
